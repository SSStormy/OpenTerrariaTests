﻿using System;
using System.Linq;
using OpenTerrariaClient;
using OpenTerrariaClient.Client;
using OpenTerrariaClient.Client.Service;
using OpenTerrariaClient.Model;
using OpenTerrariaClient.Model.ID;
using OpenTerrariaClient.Packet;

namespace OpenTerrariaTests
{
    public class BasicService : IService
    {
        private TerrariaClient _client;

        public void Install(TerrariaClient client)
        {
            _client = client;

            client.Connected += (s, e) => Console.WriteLine("Connected.");
            client.Disconnected += (s, e) => Console.WriteLine($"Disconnected: {e.Reason}");
            client.LoggedIn += (s, e) =>
            {
                Console.WriteLine("Logged in.");

                /* 
                   IMPORTANT : 
                   If your client is a command bot I recommend killing it on login.
                   This way it shouldn't interfere with monster ai and spawns.
                */

                _client.CurrentPlayer.Killme();
            };
            client.StatusReceived += (s, e) => Console.WriteLine($"Status: {e.Status.Text}");
            client.MessageReceived += (s, e) =>
            {
                Console.WriteLine($"<{client.GetExistingPlayer(e.Message.PlayerId).Appearance.Name}> {e.Message.Text}");

                if (e.Sender != MessageReceivedEventArgs.SenderType.Player) return;

                try
                {
                    // quick command hack thingy
                    string[] words = e.Message.Text.Split(' ');
                    switch (words[0])
                    {
                        case "!itemid": // !itemid <id> <amnt>
                            GiveItem(e.Player, new GameItem(short.Parse(words[1]), short.Parse(words[2])));
                            break;
                        case "!itemname": //!itemname <amnt> <name>
                            string name = string.Join(" ", words, 2, words.Length - 2);
                            GiveItem(e.Player, new GameItem(IdLookup.GetItem(name), short.Parse(words[1])));
                            break;
                        case "!goto": // !goto <username>
                            string query = string.Join(" ", words, 1, words.Length - 1).ToLowerInvariant();
                            Player target =
                                _client.Players.FirstOrDefault(
                                    p => p.Appearance.Name.ToLowerInvariant() == query);
                            _client.CurrentPlayer.SetPos(target.Position);
                            break;
                        case "!spawnboss": // !spawn <id>
                            short id = short.Parse(words[1]);
                            _client.Send(TerrPacketType.SpawnBossInvsaion,
                                new SpawnBossOrInvasion(_client.CurrentPlayer.PlayerId.Value, id));
                            break;
                    }
                }
                catch (Exception)
                {
                }
            };
        }

        private void GiveItem(Player player, GameItem item)
            => _client.Send(TerrPacketType.UpdateItemDrop,
                new WorldItem(item, 400, player.Position, 0));
        // 400 is the item id which means that we just made a new item.
    }
}