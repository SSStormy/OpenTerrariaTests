﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using OpenTerrariaClient;
using OpenTerrariaClient.Client;
using OpenTerrariaClient.Model.ID;

namespace OpenTerrariaTests
{
    internal static class Program
    {
        private static readonly Dictionary<ClassLogMessageEventArgs.LogSeverity, ConsoleColor> _colors = new Dictionary
            <ClassLogMessageEventArgs.LogSeverity, ConsoleColor>()
        {
            {ClassLogMessageEventArgs.LogSeverity.Info, ConsoleColor.Blue},
            {ClassLogMessageEventArgs.LogSeverity.Warning, ConsoleColor.Yellow},
            {ClassLogMessageEventArgs.LogSeverity.Critical, ConsoleColor.Red}
        };

        private static void Main()
        {
            dynamic config = JObject.Parse(File.ReadAllText("config.json"));
            if (config == null) throw new InvalidOperationException("Failed parsing config.json");

            TerrariaClient client = new TerrariaClient(cfg =>
            {
                cfg.Player(player =>
                {
                    player.Health(400);
                    player.Appearance(appear =>
                    {
                        appear.Name("Bot Client");
                    });
                    player.Inventory(inv =>
                    {
                        inv.AddAccessory(ItemId.PutridScent);

                        inv.SetHelmet(ItemId.DTownsHelmet);
                        inv.SetChestplate(ItemId.DTownsBreastplate);
                        inv.SetLeggings(ItemId.DTownsLeggings);
                    });
                });
            });

            Task.Run(async () =>
            {
                client.Log.MessageReceived += (s, e) => StrmyCore.Logger.FormattedWrite(e.Severity.ToString(), e.Message, _colors[e.Severity]);
                client.Services.Add<BasicService>();

                await client.ConnectAndLogin((string) config.Host, (int) config.Port);
            });

            client.Wait();

            Console.WriteLine("\r\nPress any key to continue...");
            Console.ReadLine();
        }
    }
}