# OpenTerrariaTests
An example client/tests for OpenTerraria


# Prerequisites
* OpenTerraria
* .Net 4.5.2
* Newtonsoft.Json

# Setup
* Add a project reference to ```TerrariaBridge``` for this project.
* Create a ```config.json``` inside thr application folder and fill it with data in the following format:
````
{
	"Host" : "", //ip
	"Port" : 7777
}
````
* After that, you should be able to compile and run the client.
